create or replace table articles
(
    id         int auto_increment
    primary key,
    title      varchar(255)                             not null,
    content    text                                     null,
    created_at datetime(6) default current_timestamp(6) not null
);

create or replace table users
(
    id         int auto_increment
    primary key,
    firstname  varchar(64)                              not null,
    lastname   varchar(64)                              not null,
    email      varchar(255)                             not null,
    phone      varchar(13)                              null,
    password   varchar(60)                              not null,
    is_admin   tinyint(1)                               not null,
    created_at datetime(6) default current_timestamp(6) null,
    constraint index_unique_email
    unique (email)
);

