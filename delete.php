<?php

require "./include/general.php";
require "./include/database.php";

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = mysqli_real_escape_string($conn, $_GET['id']);

    $author = mysqli_query($conn, "SELECT author_id FROM article WHERE id = '$id'");
    $author = mysqli_fetch_column($author);

    if ($author && ((int) $author == $_SESSION["id"])) {
        mysqli_query($conn, "DELETE FROM article WHERE id='$id'");
        header("Location: ./user_articles.php");
        die();
    }

    if ($_SESSION["admin"] ?? false) {
        mysqli_query($conn, "DELETE FROM article WHERE id='$id'");
    }


    header("Location: ./articles.php");
    die();
}
