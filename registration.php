<?php
require "./include/general.php";
require "./include/auth_guest.php";
require "./include/database.php";

$error = "";

// Pokud je formulář odeslaný jako POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $email = mysqli_real_escape_string($conn, $_POST["email"]);
    $result = mysqli_query($conn, "SELECT COUNT(1) FROM `users` WHERE `email` = '$email'");

    if (((int) mysqli_fetch_column($result)) === 0) {
        $firstname = mysqli_real_escape_string($conn, $_POST["firstname"]);
        $lastname = mysqli_real_escape_string($conn, $_POST["lastname"]);
        $phone = mysqli_real_escape_string($conn, $_POST["phone"]);

        $password = mysqli_real_escape_string($conn, $_POST["password"]);
        $password = password_hash($password, PASSWORD_BCRYPT);

        mysqli_query($conn, "INSERT INTO `users` (`email`, `password`, `firstname`, `lastname`, `phone`) VALUES ('$email', '$password', '$firstname', '$lastname', '$phone')");

        // TODO: přesměrovat na profil
    } else {
        $error = "Uživatel s tímto emailem již existuje!";
    }
}
?>

<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Registrace</h1>
                        <p>Na této stránce se můžete registrovat.</p>
                    </header>

                    <hr class="major" />
                </section>
                <!-- Form -->
                <form method="post" onsubmit="return checkPasswords();">
                    <div class="row gtr-uniform">
                        <div class="col-6 col-12-xsmall">
                            <p>Kontaktní údaje</p>
                            <input type="text" name="firstname" id="first-name" placeholder="Křestní jméno" required /><br>
                            <input type="text" name="lastname" id="last-name" placeholder="Příjmení" required /><br>
                            <input type="email" name="email" id="email" placeholder="E-mail" required /><br>
                            <input type="tel" name="phone" id="phone" placeholder="Telefonní číslo" pattern="\+\d{12}" required /><br>
                            <br>
                            <p>Registrační údaje</p>
                            <input type="password" name="password" id="password" placeholder="Heslo" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Musí obsahovat nejméně jednu číslici, jedno velké a malé písmeno a alespoň 8 znaků" required /><br>
                            <input type="password" name="passValidation" id="passValidation" placeholder="Potvrďte heslo" required />

                            <p id="message" style="color: red;"><?php echo $error ?></p>
                        </div>
                        <script>
                            function checkPasswords() {
                                const heslo = document.getElementById("password").value;
                                const hesloOveni = document.getElementById("passValidation").value;
                                const message = document.getElementById("message");

                                if (heslo !== hesloOveni) {
                                    message.textContent = "Zadaná hesla se neshodují";
                                    return false;
                                }

                                message.textContent = "";
                                return true;
                            }
                        </script>
                        <!-- Break -->
                        <div class="col-12">
                            <ul class="actions">
                                <li><input type="submit" value="Odeslat" class="primary" /></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>