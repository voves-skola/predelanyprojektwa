<?php
require "./include/general.php";
require "./include/admin_guard.php";
require "./include/database.php";

$users = mysqli_query($conn, "SELECT * FROM users");


?>





<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Seznam uživatelů</h1>
                        <p>Zde naleznete seznam uživatelů</p>
                    </header>
                    <hr class="major" />

                    <?php foreach ($users as $user) : ?>

                        <article>
                            <h2><?php echo $user["firstname"] . " " . $user["lastname"] ?></h2>
                            <p><?php echo $user["email"] ?></p>
                            <a href="/ondra-wa/delete_admin_user.php?id=<?php echo $user['id']  ?>" onclick="return confirm('Opravdu chcete smazat tento účet?')">Smazat</a>

                        </article>

                    <?php endforeach; ?>

                </section>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>