<?php
require "./include/general.php";
require "./include/auth_guard.php";
require "./include/database.php";

$title = "";
$content = "";
$id = null;

if (isset($_GET["id"])) {
    // edituju
    $id = mysqli_real_escape_string($conn, $_GET["id"]);

    $article = mysqli_query($conn, "SELECT * FROM article WHERE id = '$id'");
    $article = mysqli_fetch_assoc($article);

    if (!$_SESSION["admin"] && $article["author_id"] !== $_SESSION["id"]) {
        header("Location: ./form.php");
        die();
    }

    $title = $article["title"];
    $content = $article["content"];
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $title = mysqli_real_escape_string($conn, $_POST["title"]);
    $content = mysqli_real_escape_string($conn, $_POST["content"]);

    if ($id === null) {
        mysqli_query($conn, "INSERT INTO article(title, content, author_id) VALUES ('$title','$content', '{$_SESSION['id']}')");
    } else {
        mysqli_query($conn, "UPDATE `article` SET `title` = '$title', `content` = '$content' WHERE `id` = '$id'");
    }

    header("Location: ./user_articles.php");
    die();
}
?>


<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Nový dotaz</h1>
                        <p>V této sekci webu můžete zadat svůj dotaz.</p>
                    </header>

                    <hr class="major" />
                    <!-- Form -->
                    <h3>Dotaz</h3>

                    <form method="post">
                        <div class="row gtr-uniform">
                            <div class="col-6 col-12-xsmall">
                                <input type="text" name="title" id="title" placeholder="Název článku" value="<?php echo $title ?>" required />
                            </div>
                            <br>
                            <!-- Break -->
                            <div class="col-12">
                                <textarea name="content" id="content" placeholder="Zde můžete napsat příspěvek" rows="6" required><?php echo $content ?></textarea>
                            </div>
                            <!-- Break -->
                            <div class="col-12">
                                <ul class="actions">
                                    <li><input type="submit" value="Send Message" class="primary" /></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>