<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Paměti RAM</h1>

                        <p>Operační paměť neboli RAM patří mezi nejdůležitější parametry každého počítače,
                            notebooku, serveru či mobilního zařízení. Větší kapacita RAM znamená plynulejší chod programů, her,
                            ale i celého počítače. Co je operační paměť RAM, v čem se liší její varianty a dle čeho vybírat? Poradíme vám.
                        </p>
                    </header>

                    <hr class="major" />

                    <h2>Co je a k čemu slouží operační paměť RAM?</h2>
                    <p>Operační paměť RAM (Random Acces Memory), lidově též „ramka” je rychle přístupná elektronická paměť sloužící
                        procesoru pro často využívaná data právě spuštěných aplikací.
                        Na rozdíl od pevného disku nebo SSD operační paměť data neukládá, a tak se při vypnutí zařízení kompletně
                        vymaže.</p>
                    <h2>Jak zvolit dostatečnou velikost operační paměti RAM?</h2>
                    <p>Velikost operační paměti RAM ovlivňuje plynulost chodu programů, her a celého systému. Při volbě kapacity RAM
                        berte v potaz náročnost úkonů, které budete od počítače vyžadovat.</p>
                    <table>
                        <tr>
                            <th>Kapacita RAM</th>
                            <th>Optimální využití</th>
                        </tr>
                        <tr>
                            <td>
                                <p>4 GB</p>
                            </td>
                            <td>
                                <p>Základní kancelářské práce, multimédia, hraní nenáročných her.</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>8 - 16 GB</p>
                            </td>
                            <td>
                                <p>Dostatek RAM pro uspokojivé hraní většiny her a chod náročných programů.</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>32 GB</p>
                            </td>
                            <td>
                                Renderovací, modelovací a grafický software, virtualizace.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                32 GB a více
                            </td>
                            <td>
                                Práce s grafickou ve vysokém rozlišení na více monitorech, virtuální realita.
                            </td>
                        </tr>
                    </table>
                    <h2>Frekvence neboli rychlost RAM a napětí</h2>
                    <p>Frekvence paměti RAM je parametr definující její rychlost. Rychlostí paměti měříme úroveň její schopnosti číst a
                        zapisovat data. Obecně platí, že rychlejší operační paměť přináší nárůst plynulosti a výkonu. Napětí RAM
                        označuje spotřebu paměťových modulů.</p>
                    <h2>V čem se liší operační paměť DDR4, DDR3 a DDR2?</h2>
                    <p>Operační paměť DDR4, DDR3 nebo DDR2 se liší rychlostí a napětím, které označuje právě standard DDR. DDR (Double
                        Data Rate) je typ paměti nahrazující předchozí typ SD a umožňující zpracovávat data ještě rychleji.</p>
                    <table>
                        <tr>
                            <th>Typ paměti RAM</th>
                            <th>Frekvence paměti</th>
                            <th>Napětí RAM</th>
                        </tr>
                        <tr>
                            <td>Operační paměť DDR</td>
                            <td>200–400 MHz</td>
                            <td>2,5 – 2,6 V</td>
                        </tr>
                        <tr>
                            <td>Operační paměť DDR2</td>
                            <td>800–2133,33 MHz</td>
                            <td>1,5 / 1,35 V</td>
                        </tr>
                        <tr>
                            <td>Operační paměť DDR4</td>
                            <td>2133,33–4266,67 MHz</td>
                            <td>1,2 / 1,05 V</td>
                        </tr>
                    </table>
                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>