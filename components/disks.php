<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Pevné disky</h1>
                        <p>Pevné disky slouží k ukládání a zálohování dat počítače, notebooku a dalších zařízení. Dělí se na interní disky,
                            které využijete, pokud skládáte počítač nebo potřebujete rozšířit jeho úložný prostor, a externí disky. Externí
                            disky se hodí k ukládání a zálohování dat, jako jsou filmy, fotky nebo hudba. Ke stolnímu počítači, notebooku,
                            ale i televizi se zpravidla připojují prostřednictvím USB. Můžete je tak snadno odpojit i za běhu počítače,
                            televize a připojit k jinému zařízení. Na co dalšího se zaměřit při výběru pevného disku?</p>
                    </header>

                    <hr class="major" />
                    <h2>Typ disků</h2>
                    <h3>SSD disky</h3>
                    <p>Moderní disky založené na technologii NAND Flash, která k ukládání dat využívá tranzistory. Tyto disky jsou velmi
                        rychlé a neobsahují pohyblivé části, což má pozitivní vliv na odolnost vůči mechanickému poškození. Pro jejich
                        vysokou rychlost jsou dosazovány jako disky pro operační systém a nejpoužívanější aplikace.</p>
                    <h3>HDD disky</h3>
                    <p>HDD jsou tradiční disky, které data ukládají na rotující plotny pomocí magnetické indukce. Jejich největší
                        výhodou je nízká cena oproti SSD, jsou ale výrazně pomalejší. V moderních sestavách hájí svoje osazení na pozici
                        úložiště pro data nebo málo používané programy.</p>
                    <h3>SSHD disky</h3>
                    <p>SSHD kombinují obě populární technologie. Jediné pouzdro obsahuje hlavní HDD, kterému sekunduje menší SSD
                        sloužící jako velká cache paměť. Do ní jsou data přesouvána automaticky podle četnosti jejich využívání. Jedná
                        se o dobrý kompromis, například pokud máte v notebooku pouze jednu pozici a chcete co nejrychlejší pevný disk.
                    </p>
                    <h2>Formát</h2>
                    <ul>
                        <li><b>3,5"</b> – standardní formát pevných disků používaný ve většině desktopových a serverových nasazení.</li>
                        <li><b>2,5"</b> – zmenšený formát disků, který se používá především v noteboocích, nic ale obvykle nebrání ani
                            dosazení
                            do standardního počítače. Menší rozměry jsou obvykle vykoupeny vyšší cenou nebo nižší rychlostí.</li>
                        <li><b>M.2</b> – tento moderní formát byl vytvořen pro SSD disky. Ty se v jeho případě usazují přímo do
                            specializovaného slotu na základní desce. Největší výhodou jsou miniaturní rozměry a rychlostní potenciál
                            daleko přesahující starší řešení.</li>
                    </ul>
                    <h2>Rychlost disků: čtení a zápis</h2>
                    <p>Rychlost čtení disku je rychlost, jakou je zařízení schopno zpřístupnit uložená data. Zápis naopak říká, jak
                        rychle disk dokáže data přijmout a uložit. Na poli rychlosti jednoznačně dominují SSD disky nad disky
                        plotnovými.</p>
                    <ul>
                        <li><b>Rychlost otáček HDD</b> – tento parametr udává, jak rychle se mechanické pevné disky otáčí. Jedná se o
                            jeden
                            z
                            hlavních faktorů majících zásadní vliv na rychlost disku. Počet otáček je přímo úměrný rychlosti, ale
                            také
                            hlučnosti a nepřímo úměrný životnosti.</li>
                        <li><b>Vyrovnávací paměť disku</b> – vyrovnávací paměť neboli cache je rychlá paměť uvnitř pevného disku. Jejím
                            úkolem je zrychlit práci pevného disku uchováním vybraných dat. Vyšší vyrovnávací paměť se na rychlosti
                            disku projeví zpravidla pozitivně.</li>
                    </ul>
                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>