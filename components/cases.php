<?php
require "./../include/general.php";
?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./../include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="./../index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>PC skříně, Case</h1>
                        <p>PC case neboli počítačová skříň je při stavbě nového či vylepšování stávajícího počítače jednou z
                            nejdůležitějších komponent. Zohledňuje se především její umístění a to, na co budete sestavený počítač využívat.
                            Abychom výběr ulehčili, připravili jsme krátkého průvodce, který vám s výběrem PC skříně poradí.</p>
                    </header>

                    <hr class="major" />
                    <p>PC skříně se liší zejména velikostí, která předurčuje kompatibilitu s PC komponenty, ale i vnitřní cirkulaci
                        vzduchu. Základní rozdělení velikostí skříní úzce souvisí s typem podporované základní desky. Nejčastěji se
                        setkáme s PC case typu Big Tower, Midi Tower a Mini Tower.</p>
                    <img src="./../obrazky/PC-case-big.jpg" alt="Velká počítačová skříň">
                    <img src="./../obrazky/PC-case-midi.jpg" alt="PC case midi tower">
                    <img src="./../obrazky/PC-case-mini.jpg" alt="Počítačová skříň Mini tower">
                    <h2>Big Tower je největší PC case</h2>
                    <p>Big Tower PC case je díky svým rozměrům ideální volbou pro nejnáročnější uživatele. Nabízí dostatek místa pro
                        největší základní desky typu eATX a ATX, vysoký počet větráků a několik pevných disků. PC skříň Big Tower
                        přináší dostatek místa pro přehledné vedení kabelů i budoucí rozšíření počítače.</p>
                    <h2>Midi Tower – střední cesta</h2>
                    <p>Počítačová skříň Midi Tower patří mezi nejrozšířenější typy. Přináší kompromis mezi prostorem pro komponenty,
                        cirkulací vzduchu a samotnou velikostí skříně. Podporuje základní desky ATX či menší.</p>
                    <h2>Mini Tower je PC case pro nenáročné</h2>
                    <p>Mini Tower PC case s přehledem pojme všechny důležité komponenty. Díky menším rozměrům se však nehodí pro výkonné
                        stroje. Na základní desky mATX nebo mITX nepřipojíte herní grafickou kartu ani několik pevných disků.</p>

                </section>

            </div>
        </div>

        <?php include "./../include/side_nav.php"; ?>

    </div>

    <?php include "./../include/scripts.php"; ?>
</body>

</html>