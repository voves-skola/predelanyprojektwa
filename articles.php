<?php
require "./include/general.php";
require "./include/database.php";

$articles = mysqli_query($conn, "SELECT * FROM article");


?>





<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<?php include "./include/head.php"; ?>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="index.php" class="logo"><strong>Vše o PC</strong> - Ondřej Voves</a>
                </header>

                <section>
                    <header class="main">
                        <h1>Seznam článků</h1>
                        <p>Zde naleznete veškteré články</p>
                    </header>
                    <hr class="major" />

                    <?php foreach ($articles as $article) : ?>

                        <article>
                            <h2><?php echo $article["title"] ?></h2>
                            <p><?php echo $article["content"] ?></p>

                            <?php if ($_SESSION["admin"] ?? false) : ?>

                                <a href="/ondra-wa/form.php?id=<?php echo $article['id']  ?>">Upravit</a>
                                <a href="/ondra-wa/delete.php?id=<?php echo $article['id']  ?>" onclick="return confirm('Opravdu chcete smazat tento účet?')">Smazat</a>
                            <?php endif; ?>
                        </article>

                    <?php endforeach; ?>

                </section>

            </div>
        </div>

        <?php include "./include/side_nav.php"; ?>

    </div>

    <?php include "./include/scripts.php"; ?>
</body>

</html>